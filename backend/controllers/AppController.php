<?php

namespace backend\controllers;

use Yii;
use common\models\App;
use common\models\AppSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AppController implements the CRUD actions for App model.
 */
class AppController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['create','update','view', 'index','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all App models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single App model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new App model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new App();

        if ($model->load(Yii::$app->request->post()) ) {
            $uploadedLogo = UploadedFile::getInstance($model, 'img_logo');
            $uploadedBanner = UploadedFile::getInstance($model, 'img_banner');
            if($uploadedLogo != null) {//xu ly file thumbnail
                $file = time() . "_" . $uploadedLogo->name;
                $uploadPath = Yii::getAlias('@webroot') . '/uploads/img/' . $file;
                $url = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
                $mime = $uploadedLogo->type;
                //extensive suitability check before doing anything with the file�
                $message = '';
                if ($uploadedLogo == null) {
                    $message = "No file uploaded.";
                } else if ($uploadedLogo->size == 0) {
                    $message = "The file is of zero length.";
                } else if ($mime != "image/jpeg" && $mime != "image/png") {
                    $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
                } else if ($uploadedLogo->tempName == null) {
                    $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
                } else {
                    $move = $uploadedLogo->saveAs($uploadPath);
                    if (!$move) {
                        $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
                    }
                }

                if (!empty($message)) {
                    $model->addError('img_logo', $message);
                }
                $model->img_logo = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
            }

            if($uploadedBanner != null) {//xu ly file slide
                $file = time() . "_" . $uploadedBanner->name;
                $uploadPath = Yii::getAlias('@webroot') . '/uploads/img/' . $file;
                $url = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
                $mime = $uploadedBanner->type;
                //extensive suitability check before doing anything with the file�
                $message = '';
                if ($uploadedBanner == null) {
                    $message = "No file uploaded.";
                } else if ($uploadedBanner->size == 0) {
                    $message = "The file is of zero length.";
                } else if ($mime != "image/jpeg" && $mime != "image/png") {
                    $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
                } else if ($uploadedBanner->tempName == null) {
                    $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
                } else {
                    $move = $uploadedBanner->saveAs($uploadPath);
                    if (!$move) {
                        $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
                    }
                }

                if (!empty($message)) {
                    $model->addError('img_banner', $message);
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                $model->img_banner = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
            }

            if($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUrl()
    {
        $uploadedFile = UploadedFile::getInstanceByName('upload');
        $file = time()."_".$uploadedFile->name;
        //$url = Yii::$app->urlManager->createAbsoluteUrl('/uploads/ckeditor/'.$file);
        $url = Yii::$app->params['be_domain'] . '/uploads/ckeditor/'.$file;
        $uploadPath = Yii::getAlias('@webroot').'/uploads/ckeditor/'.$file;
        $mime = $uploadedFile->type;
        //extensive suitability check before doing anything with the file�
        if ($uploadedFile==null)
        {
            $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
            $message = "The file is of zero length.";
        }
        else if ($mime!="image/jpeg" && $mime!="image/png")
        {
            $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        }
        else if ($uploadedFile->tempName==null)
        {
            $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
            $message = "";
            $move = $uploadedFile->saveAs($uploadPath);
            if(!$move)
            {
                $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
            }
        }
        $funcNum = $_GET['CKEditorFuncNum'] ;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }

    /**
     * Updates an existing App model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tmpLogo = $model->img_logo;
        $tmpBanner = $model->img_banner;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedLogo = UploadedFile::getInstance($model, 'img_logo');
            $uploadedBanner = UploadedFile::getInstance($model, 'img_banner');
            if($uploadedLogo != null) {//xu ly file thumbnail
                $file = time() . "_" . $uploadedLogo->name;
                $uploadPath = Yii::getAlias('@webroot') . '/uploads/img/' . $file;
                $url = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
                $mime = $uploadedLogo->type;
                //extensive suitability check before doing anything with the file�
                $message = '';
                if ($uploadedLogo == null) {
                    $message = "No file uploaded.";
                } else if ($uploadedLogo->size == 0) {
                    $message = "The file is of zero length.";
                } else if ($mime != "image/jpeg" && $mime != "image/png") {
                    $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
                } else if ($uploadedLogo->tempName == null) {
                    $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
                } else {
                    $move = $uploadedLogo->saveAs($uploadPath);
                    if (!$move) {
                        $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
                    }
                }

                if (!empty($message)) {
                    $model->addError('img_logo', $message);
                }
                $tmpLogo = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
            }

            if($uploadedBanner != null) {//xu ly file slide
                $file = time() . "_" . $uploadedBanner->name;
                $uploadPath = Yii::getAlias('@webroot') . '/uploads/img/' . $file;
                $url = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
                $mime = $uploadedBanner->type;
                //extensive suitability check before doing anything with the file�
                $message = '';
                if ($uploadedBanner == null) {
                    $message = "No file uploaded.";
                } else if ($uploadedBanner->size == 0) {
                    $message = "The file is of zero length.";
                } else if ($mime != "image/jpeg" && $mime != "image/png") {
                    $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
                } else if ($uploadedBanner->tempName == null) {
                    $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
                } else {
                    $move = $uploadedBanner->saveAs($uploadPath);
                    if (!$move) {
                        $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
                    }
                }

                if (!empty($message)) {
                    $model->addError('img_banner', $message);
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                $tmpBanner = Yii::$app->params['be_domain'] . '/uploads/img/' . $file;
            }

            $model->img_logo = $tmpLogo;
            $model->img_banner = $tmpBanner;
            if($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing App model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the App model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return App the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = App::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
