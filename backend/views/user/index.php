<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'full_name',
            //'ascii_name',
            //'type',
            // 'password_hash',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'email:email',
            // 'email_verified:email',
            // 'address',
            // 'auth_key',
            // 'password_reset_token',
            // 'phone_number',
            // 'avartar',
            // 'cover_photo',
            // 'role',
            // 'facebook_id',
            // 'facebook_data:ntext',
            // 'google_id',
            // 'google_data:ntext',
            // 'content_count',
            // 'follower_count',
            // 'followee_count',
            // 'balance',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
