<?php

use dosamigos\ckeditor\CKEditor;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\App */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="app-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL,
        'fullSpan' => 12,
        'formConfig' => [
            'showLabels' => true,
            'labelSpan' => 2,
            'deviceSize' => ActiveForm::SIZE_SMALL,
        ],
        'options' => ['enctype' => 'multipart/form-data'],]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 200]) ?>

    <?php if(!empty($model->img_logo)) {?>
        <span style="margin-left: 210px">
        <?= Html::img($model->img_logo,['height'=>120,'width'=> 120])?>
        </span>
    <?php }?>
    <?= $form->field($model, 'img_logo')->widget(FileInput::className(), ['pluginOptions' => ['showUpload' => false]]) ?>

    <?php if(!empty($model->img_banner)) {?>
        <span style="margin-left: 210px">
        <?= Html::img($model->img_banner,['height'=>120,'width'=> 350])?>
        </span>
    <?php }?>
    <?= $form->field($model, 'img_banner')->widget(FileInput::className(), ['pluginOptions' => ['showUpload' => false]]) ?>


    <?= $form->field($model, 'feature')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=app/url'
        ]
    ]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=app/url'
        ]
    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'keyword')->textInput() ?>

    <?= $form->field($model, 'link_apk')->textInput() ?>

    <?= $form->field($model, 'link_ipk')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['app/index'], [
            'class' => 'btn btn-warning',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
