<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\App */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'title',
            [
                'label' => 'img_logo',
                'format' => 'html',
                'value' => '<img src =' . $model->img_logo .' height="80px" width="120px">',
            ],
            [
                'label' => 'img_banner',
                'format' => 'html',
                'value' => '<img src =' . $model->img_banner .' height="80px" width="120px">',
            ],
            'feature:html',
            'content:html',
            'weight',
            'link_apk:ntext',
            'link_ipk:ntext',
            'description:ntext',
            'keyword:ntext',

        ],
    ]) ?>

</div>
