<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AppSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create App', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'title',
            [
                'attribute' => 'img_logo',
                'format' => 'html',
                'value' => function ($data) {
                    if(!empty($data->img_logo))
                        return Html::img($data->img_logo,['width' => '120px']);
                    else return '';
                },
                'filter'=>false,
            ],
            [
                'attribute' => 'img_banner',
                'format' => 'html',
                'value' => function ($data) {
                    if(!empty($data->img_banner))
                        return Html::img($data->img_banner,['width' => '120px']);
                    else return '';
                },
                'filter'=>false,
            ],
            //'content:html',
            'description:ntext',
            // 'created_by',
            // 'created_at',
            // 'updated_at',
            // 'updated_by',
            // 'status',
            // 'view_count',
            // 'weight',
            // 'img_banner:ntext',
             'link_apk:ntext',
             'link_ipk:ntext',
             'keyword:ntext',
            // 'feature:html',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
