<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "app".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $img_logo
 * @property string $content
 * @property string $description
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $status
 * @property integer $view_count
 * @property integer $weight
 * @property string $img_banner
 * @property string $link_apk
 * @property string $link_ipk
 * @property string $keyword
 * @property string $feature
 */
class App extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'link_apk', 'link_ipk', 'link_apk', 'link_ipk'], 'required'],
            [['img_logo', 'content', 'description', 'img_banner', 'link_apk', 'link_ipk', 'keyword', 'feature'], 'string'],
            [['created_by', 'created_at', 'updated_at', 'updated_by', 'status', 'view_count', 'weight'], 'integer'],
            [['name', 'title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'img_logo' => 'Img Logo',
            'content' => 'Content',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'view_count' => 'View Count',
            'weight' => 'Weight',
            'img_banner' => 'Img Banner',
            'link_apk' => 'Link Apk',
            'link_ipk' => 'Link Ipk',
            'keyword' => 'Keyword',
            'feature' => 'Feature',
        ];
    }
}
