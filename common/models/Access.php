<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "access".
 *
 * @property integer $id
 * @property integer $app_id
 * @property string $app_name
 * @property string $ip_client
 * @property string $agent
 * @property string $device_type
 * @property integer $access_date
 * @property string $str_date
 *
 * @property App $app
 */
class Access extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['app_id', 'access_date'], 'integer'],
            [['device_type'], 'string', 'max' => 100],
            [['app_name','agent', 'ip_client','str_date'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'app_id' => 'App ID',
            'app_name' => 'App Name',
            'ip_client' => 'Ip Client',
            'agent' => 'Agent',
            'access_date' => 'Access Date',
            'device_type' => 'Device type',
            'str_date' => 'String date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApp()
    {
        return $this->hasOne(App::className(), ['id' => 'app_id']);
    }
}
