<?php

/* @var $this yii\web\View */
/* @var $app app_one\models\AppDetail */

$this->title = $app->name;
?>
<div id="home" class="banner">
    <div class="container">
        <div class="header">
            <div class="logo">
                <a href="/#"><img src="<?= $app->img_logo?>" width="96" alt=""></a>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="banner-info">
            <h1><?= $app->name?></h1>
            <p><?= $app->title?></p>

            <iframe src="//www.facebook.com/plugins/like.php?href=http://playboxhd.net/%2F&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px; vertical-align: top; width: 100%;" allowtransparency="true"></iframe>
        </div>


    </div>
    <div class="hand">
        <img src="<?= $app->img_banner?>" alt="">
    </div>
</div>

<!--features-->
<div id="feature" class="features">
    <div class="container" style="color: #1ce4ef">
        <div class="feature-info text-center">
            <h3>APP FEATURES</h3>
            <?= $app->feature?>
        </div>

    </div>
</div>
<!--landing-->

<div id="ios-info" class="landing">
    <h3>Description and support</h3>
    <div class="alnding-sec">
        <div class="col-md-6 landing-info">
            <?= $app->content?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div id="comments" class="landing">

    <div id="fb-root" class=" fb_reset"></div>
    <fb:comments href="http://vivas.vn" colorscheme="light" numposts="1" width="100%" fb-xfbml-state="rendered" class="fb_iframe_widget fb_iframe_widget_fluid">
        <span style="height: 2749px;">
            <iframe id="f1104c8b4c" name="f2d7589ddc" scrolling="no" title="Facebook Social Plugin" class="fb_ltr fb_iframe_widget_lift" src="https://www.facebook.com/plugins/comments.php?api_key=&amp;channel_url=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df3b31e36c%26domain%3Dvivas.vn%26origin%3Dhttp%253A%252F%252Fvivas.vn%252Ff194e41598%26relation%3Dparent.parent&amp;colorscheme=light&amp;href=http://vivas.vn&amp;locale=en_US&amp;numposts=1&amp;sdk=joey&amp;skin=light&amp;width=100%25" style="border: none; overflow: hidden; height: 2749px; width: 100%;"></iframe></span>
    </fb:comments>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        var str = '<fb:comments href="http://vivas.vn" colorscheme="light" numposts="1" width="100%"></fb:comments>';
        document.write(str);

    </script>
</div>
<div class="clearfix"></div>
<div style="bottom: 0;position: fixed;z-index: 999999;right: 10px;">
    <a href="<?= $app->link_apk?>"><img src="<?=Yii::$app->request->baseUrl?>/images/download-apk.png" height="40px" width="120px"></a>
    &nbsp;<a href="<?= $app->link_apk?>"><img src="<?=Yii::$app->request->baseUrl?>/images/download-ipk.png" height="40px" width="120px"></a>
</div>