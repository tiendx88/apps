<?php
namespace app_one\controllers;

use app_one\models\AppDetail;
use common\models\Access;
use common\models\App;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
    var $detect = null;
    var $deviceType = null;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function __construct($id, $module) {
        parent::__construct($id, $module);
        $this->detect = new \Mobile_Detect();


        if ($this->detect->isMobile()) {
            if ($this->detect->isTablet() || $this->detect->is('AndroidOS')) {
                if($this->detect->is('iOS')) {
                    $this->deviceType = 'IOS';
                } else {
                    $this->deviceType = 'Android';
                }
            } else if($this->detect->is('iOS')){
                $this->deviceType = 'IOS';
            } else if($this->detect->is('Blackberry')){
                $this->deviceType = 'Blackberry';
            } else {
                $this->deviceType = 'Other';
            }
        } else {
            $this->deviceType = 'Web PC';
        }
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $app = App::findOne(['id'=> AppDetail::APP_ID]);
        if(empty($app)) {
            echo "<span>Application is not exist!</span>";
            exit;
        }
        $app->view_count += 1;
        $app->save();
        $agent = Yii::$app->request->getUserAgent();
        $ipClient = $this->getUserIP();
        $appId = AppDetail::APP_ID;
        $access = new Access();
        $access->app_id = $appId;
        $access->app_name = AppDetail::APP_NAME;
        $access->ip_client = $ipClient;
        $access->agent = $agent;
        $access->access_date = time();
        $access->str_date = date('d-m-Y', time());
        $access->device_type = $this->deviceType;
        $access->save();
        $cookies = Yii::$app->response->cookies;
        // add a new cookie to the response to be sent
        $cookies->add(new Cookie([
            'name' => 'keywords',
            'value' => $app->keyword,
        ]));

        $cookies->add(new Cookie([
            'name' => 'description',
            'value' => $app->description,
        ]));

        return $this->render('index', ['app' => $app]);
    }

    /**
     * get User Ip
     * @return string
     */
    public function getUserIP() {
        if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
                $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($addr[0]);
            } else {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

}
